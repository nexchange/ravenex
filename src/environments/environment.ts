// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://192.168.8.122:8888/api/v1', // Backend API from Postman Ravenex
  defaultMenuType: 'menu-sub-hidden',
  subHiddenBreakpoint: 1440,
  menuHiddenBreakpoint: 768,
  themeColorStorageKey: 'themecolor',
  isMultiColorActive: true,
  /* Color Options: 'light', 'dark' */
  defaultColor: 'light',
  isDarkSwitchActive: true,
  defaultDirection: 'ltr',
  themeRadiusStorageKey: 'themeradius',
};
