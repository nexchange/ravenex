export interface User {
    id: number;
    user_id: string;
    email: string;
    mobile: any;
    password: string;
    verified: number;
    date_created: any;
    date_modified: any;
    user_first_name: string;
    user_last_name: string;
    user_gender: string;
    user_dob: any;
    user_country: string;
    user_image: any;
    token: string;
}
