import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorComponent } from '../views/error/error.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { HelpersModule } from './helpers/helpers.module';
import { ModelsModule } from './models/models.module';
import { ServicesModule } from './services/services.module';

@NgModule({
  declarations: [ErrorComponent],
  imports: [
    RouterModule,
    CommonModule,
    TranslateModule,
    PerfectScrollbarModule,
    HelpersModule,
    ModelsModule,
    ServicesModule,
  ],
  exports: [
    PerfectScrollbarModule,
    RouterModule,
    ErrorComponent,
    TranslateModule,
    HelpersModule,
    ModelsModule,
    ServicesModule,
    CommonModule
  ]
})
export class SharedModule { }
