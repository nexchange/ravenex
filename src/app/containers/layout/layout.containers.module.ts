import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TopnavComponent } from './topnav/topnav.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CollapseModule, BsDropdownModule, TooltipModule } from 'ngx-bootstrap';
import { FooterComponent } from './footer/footer.component';
import { ApplicationMenuComponent } from './application-menu/application-menu.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    TopnavComponent,
    SidebarComponent,
    FooterComponent,
    ApplicationMenuComponent
  ],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    TranslateModule,
    RouterModule,
    CollapseModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
  ],
  exports: [
    TopnavComponent,
    SidebarComponent,
    FooterComponent,
    ApplicationMenuComponent
  ]
})
export class LayoutContainersModule { }
