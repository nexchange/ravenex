import { Component, OnInit } from '@angular/core';
import { CommonModule } from "@angular/common";
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})

export class HomeComponent implements OnInit {
  public rates:any = [] 
  public keys:any = []
  public values:any = []
 
  constructor(private http: HttpClient) { }

  getRate(){
    this.http.get(`${environment.apiUrl}/rates`).subscribe((res)=>{
      this.rates = res
      this.keys = Object.keys(this.rates);
      this.values = Object.values(this.rates)
      // let map = new Map(Object.entries(this.rates));
      // Object.entries(this.rates).forEach(([key, value]) => console.log(`${key}: ${value}`));
      // console.log(this.rates)
      // console.log(this.keys)
      console.log(this.values)      
      // console.log(map)
      // console.log(this.rates['GHSGHS']['rate']);
    })
  }

  ngOnInit(): void {
    this.getRate();
  }

  transactionID = Math.floor((Math.random() * 10000000) + 1);

}
