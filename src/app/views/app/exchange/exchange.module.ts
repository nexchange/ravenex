import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { ExchangeComponent } from './exchange.component';
import { ExchangeRoutingModule } from './exchange.routing'
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { ArchwizardModule } from 'angular-archwizard';



@NgModule({
  declarations: [ExchangeComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgSelectModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    ExchangeRoutingModule,
    LayoutContainersModule,
    ArchwizardModule
  ],
  exports: [
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule
  ]
})
export class ExchangeModule { }
