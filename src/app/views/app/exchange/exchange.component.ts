import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
})

export class ExchangeComponent implements OnInit {
  public currencies:any = []
  public rates:any = []

  selectedFrom = this.rates.from;
  selectedTo = this.rates.to;
  exchangeRate;
  fromAmount;
  toAmount;

  constructor(private http: HttpClient) { 
  }

  getCurrency(){
    this.http.get(`${environment.apiUrl}/currencies`).subscribe((res)=>{
     this.currencies = res
    })
  }

  getRate(){
    this.http.get(`${environment.apiUrl}/rates`).subscribe((res)=>{
      this.rates = res
      console.log(this.rates)
      // console.log(this.rates['GHSGHS']['rate']);
    })
  }

  ngOnInit(): void {
    this.getCurrency();
    this.getRate();
  }

  fromExchange(fromAmount) {
    if (this.selectedFrom == this.selectedTo) { 
      this.toAmount = fromAmount;
    } else {
      this.toAmount = this.rates[this.selectedFrom.concat(this.selectedTo.toString())]['rate'] * fromAmount
    }
  }

  toExchange(toAmount) {
    if (this.selectedFrom == this.selectedTo) { 
      this.fromAmount = toAmount;
    } else {
      this.fromAmount = toAmount/this.rates[this.selectedFrom.concat(this.selectedTo.toString())]['rate']
    }
  }
}
