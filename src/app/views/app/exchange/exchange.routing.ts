import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExchangeComponent } from './exchange.component';

const routes: Routes = [
    {
        path: '', component: ExchangeComponent,
        children: [
            { path: '', component: ExchangeComponent },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExchangeRoutingModule { }
