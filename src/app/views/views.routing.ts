import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewsComponent } from './views.component';
import { ErrorComponent } from './error/error.component';
import { AuthGuard } from 'src/app/shared/helpers';
import { environment } from './../../environments/environment';

const appModule = () => import('./app/app.module').then(m => m.AppModule);
const userModule = () => import('./user/user.module').then(m => m.UserModule);

let routes: Routes = [
  { path: '', component: ViewsComponent, pathMatch: 'full' },
  { path: 'app', loadChildren: appModule },
  // { path: 'app', loadChildren: appModule, canActivate: [AuthGuard] },
  { path: 'user', loadChildren: userModule },
  { path: 'error', component: ErrorComponent },
  { path: '**', redirectTo: '/error' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ViewRoutingModule { }
